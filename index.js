//true = X, false = O
var current_player = true;
var play_map = new Map();

function create_board() {
  updateCurrPlayerHeading("Xs' Turn ", 0);
  let board = document.getElementById("board");
  for (let i = 0; i < 9; i++) {
    let section = document.createElement("div");
    section.className = "section";
    section.id = i + 1;
    section.onclick = function place_marker() {
      let player = "";
      current_player == true ? (player = "X") : (player = "O");
      let section_selected = document.getElementById(section.id);
      if (section_selected.textContent.length == 0) {
        current_player === true
          ? (section_selected.textContent = "X")
          : (section_selected.textContent = "O");

        // player change
        current_player = !current_player;
        checkWin(player, section.id);
      }
    };
    board.appendChild(section);
  }
}

function updateCurrPlayerHeading(message, count) {
  let heading = document.getElementById("current-player");
  if (play_map.size === 9 && count != 3) {
    heading.textContent = "Draw";
  } else {
    heading.textContent = message;
  }
}

function checkWin(player, section) {
  //these are all the possible wins for any player
  const wins = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7],
  ];
  //filter all the possible wins that include the current selected option
  let possible_wins = wins.filter((element) =>
    element.includes(Number(section))
  );

  //add the selected tic tac toe dection to the map,
  //only options that havent been chosen before will be added to map
  //map has the number 1-9 selected as the key and the value is the player who selected it
  play_map.set(Number(section), player);

  //once at least 3 selections have been made, we will check for a winner
  if (play_map.size > 2) {
    //loop through the filtered possible wins
    possible_wins.forEach((element) => {
      //if any of the filtered options has all three of the same player values, then there isn a winner
      let count = 0;
      //loop through each filtered array
      element.forEach((input) => {
        //check the map to see how many of the values in each array
        //belong to the player who just made a selection and increase the count
        if (play_map.get(input) === player) {
          count += 1;
        }
      });
      //if the count is 3, then proceed with displaying the winner
      if (count === 3) {
        console.log("element", element);
        //looping through the array that has the winning selections
        //and changing their background color to display winning set
        element.forEach((section) => {
          let sectionElement = document.getElementById(section.toString());
          sectionElement.style.backgroundColor = "#e20074";
          sectionElement.style.color = "white";
        });
        updateCurrPlayerHeading("Player " + player + " wins", count);
      }
    });
  }
}
